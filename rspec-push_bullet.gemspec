require_relative 'lib/rspec/push_bullet/version'

Gem::Specification.new do |spec|
  spec.name          = "rspec-push_bullet"
  spec.version       = RSpec::PushBullet::VERSION
  spec.authors       = ["Gary Taylor"]
  spec.email         = ["gary.taylor@hismessages.com"]

  spec.summary       = %q{Send key messages to push bullet from your rspec suite}
  spec.description   = %q{Send key messages to push bullet from your rspec suite}
  spec.homepage      = "https://gitlab.com/garytaylor/rspec-push-bullet"
  spec.license       = "MIT"
  spec.required_ruby_version = Gem::Requirement.new(">= 2.3.0")

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://gitlab.com/garytaylor/rspec-push-bullet"
  spec.metadata["changelog_uri"] = "https://gitlab.com/garytaylor/rspec-push-bullet/CHANGELOG.md"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_dependency 'rspec'
end
