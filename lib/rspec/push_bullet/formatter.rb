module RSpec
  module PushBullet
    class Formatter
      RSpec::Core::Formatters.register self, :example_failed, :example_passed, :close, :start

      def initialize(output, notifier: Notifier::Pushover.new)
        self.passed = 0
        self.failed = 0
        self.pending = 0
        self.notify_on_first_failure = true
        self.notifier = notifier
      end

      def example_failed(notification)
        @failed += 1
        notifier.call("#{notification.description}\n\n#{notification&.exception&.message}", title: 'RSpec starting to fail') if failed == 1 && notify_on_first_failure

      end

      def example_passed(notification)
        @passed += 1
      end

      def close(notification)
        notifier.call('RSpec suite finished', title: 'RSpec finished')
      end

      def start(notification)
        self.total_examples = notification.count
      end

      private

      attr_accessor :total_examples, :passed, :failed, :pending, :notifier, :notify_on_first_failure

    end
  end
end
