require_relative '../push_bullet'
require "rspec/push_bullet/notifier/pushover"
require "rspec/push_bullet/formatter"

module RSpec
  module PushBullet
    module TeamCityFormatterExtension
      def initialize(*args)
        super(*args)
        self.additional_formatter = ::RSpec::PushBullet::Formatter.new(output)
      end
      def example_failed(notification)
        additional_formatter.example_failed(notification)
        super
      end

      def example_passed(notification)
        additional_formatter.example_passed(notification)
        super
      end

      def close(notification)
        additional_formatter.close(notification)
        super
      end

      def start(notification)
        additional_formatter.start(notification)
        super
      end

      private

      attr_accessor :additional_formatter
    end
  end
end

class Spec::Runner::Formatter::TeamcityFormatter
  prepend RSpec::PushBullet::TeamCityFormatterExtension




end
