module RSpec
  module PushBullet
    module Notifier
      class PushBullet
        def initialize
          @enabled = false
          return if `which pb`.empty?

          @enabled = true
        end

        def call(message, title: 'RSpec Suite')
          return unless enabled

          `pb push --title #{Shellwords.escape(title)} #{Shellwords.escape(message)}`
        end

        private

        attr_reader :enabled

      end
    end
  end
end
