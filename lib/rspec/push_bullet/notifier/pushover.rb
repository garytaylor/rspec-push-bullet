require "net/https"
module RSpec
  module PushBullet
    module Notifier
      class Pushover
        def initialize(token: ENV['PUSHOVER_API_TOKEN'], user_key: ENV['PUSHOVER_USER_KEY'], url: "https://api.pushover.net/1/messages.json")
          @token = token
          @user_key = user_key
          @url = url
        end

        def call(message, title: 'RSpec Suite')
          return unless enabled

          uri = URI.parse(url)
          req = Net::HTTP::Post.new(uri.path)
          req.set_form_data({
                              token: token,
                              user: user_key,
                              title: title,
                              message: message,
                            })
          res = Net::HTTP.new(uri.host, uri.port)
          res.use_ssl = true
          res.verify_mode = OpenSSL::SSL::VERIFY_PEER
          res.start {|http| http.request(req) }

        end

        private

        def enabled
          !token.nil? && !user_key.nil?
        end

        attr_reader :token, :user_key, :url

      end
    end
  end
end
